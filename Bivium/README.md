Bivium.alg program encodes the Bivium keystream generator which produced 200 keystream bits (with initialization step).
The translation of this program results in Bivium.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 177, in particular:
 - variables number 1 .. 93 encode values of cells of the first register of length 93 (regA);
   variable number 1 corresponds to cell regA[0], ..., variable number 93 corresponds to cell regA[92]. 
 - variables number 94 .. 177 encode values of cells of the second register of length 84 (regB);
   variable number 94 corresponds to cell regB[0], ..., variable number 177 corresponds to cell regB[83].

Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in Bivium.cnf keystream bits are encoded with variables number 1842, ..., 2041,
where variable number 1842 encodes 1st keystream bit, ... , variable number 2041 encodes 200th keystream bit.

Bivium_no_init.alg program encodes the Bivium keystream generator which produced 200 keystream bits (without initialization step).
The translation of this program results in Bivium_no_init.cnf CNF-formula.
As it done before the initial state of the register cells is encoded with the first 177 variables with numbers 1 .. 177.
Keystream output is encoded with the last 200 variables with variables number 443 .. 642,
where variable number 443 encodes 1st keystream bit and variable number 642 encodes 200th keystream bit.

Transalg do not substitute into Bivium.cnf and Bivium_no_int.cnf the initial state of the register cells (key value, initial vector) and the bits of the key stream.
To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
