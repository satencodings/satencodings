tresh.alg program encodes the Tresh keystream generator which produced 150 keystream bits.
The translation of this program results in tresh_150.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 72, in particular:
 - variables number 1 .. 11 encode values of cells of the first register (regA), 
   variable number 1 corresponds to cell regA[0], ..., variable number 11 corresponds to cell regA[10];
 - variables number 12 .. 24 encode values of cells of the second register(regB);
   variable number 12 corresponds to cell regB[0], ..., variable number 24 corresponds to cell regB[12];
 - variables number 25 .. 39 encode values of cells of the third register (regC);
   variable number 25 corresponds to cell regC[0], ..., variable number 39 corresponds to cell regC[14];
- variables number 40 .. 55 encode values of cells of the fourth register(regD);
   variable number 40 corresponds to cell regD[0], ..., variable number 55 corresponds to cell regD[15];
 - variables number 56 .. 72 encode values of cells of the fifth register (regE);
   variable number 56 corresponds to cell regE[0], ..., variable number 72 corresponds to cell regE[16];

Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in tresh_150.cnf keystream bits are encoded with variables number 751, ..., 900, 
where variable number 751 encodes 1st keystream bit, ..., variable number 900 encodes 150th keystream bit.

To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
Secret key for this problem is the assignment of first 72 variables that can be extracted from the satisfying assignment of the CNF.