Wolfram.alg program describes 128 steps of the Wolfram generator which produced 128 keystream bits.
The translation of this program results in Wolfram.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 128, in particular:
 variable number 1 corresponds to cell reg[0], variable number 2 corresponds to cell reg[1], ..., variable number 128 - to cell reg[127].

Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in Wolfram.cnf keystream bits are encoded with variables number 12417, ..., 12544, 
where variable number 12417 encodes 1st keystream bit, ... , variable number 12544 encodes 128th keystream bit.

To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
Secret key for this problem is the assignment of the first 128 variables that can be extracted from the satisfying assignment of the CNF.
