MD5 collision attack using Wang's differential paths.

We based our approach on the method proposed by X. Wang for finding two-block collisions for the MD5 hash function. It implies finding two pairs of message blocks, so we implement it using two TA-programs. Each program corresponds to the search for one pair of blocks (M0 and M0' or M1 and M1') and includes all necessary constraints on the differential path, and also most part of the bit conditions.

The TA-program for searching first pair of blocks (MD5_collision_first_block.alg) uses standard initialization values (IV) for registers cells A1, B1, C1, D1 (and the same values for A2, B2, C2, D2), since it corresponds to computing the MD5 hash value for message blocks M0 and M0'. The result of the translation for this program is MD5_first_block.cnf CNF, in which the first 512 variables encode the first message block M0 and the last 256 variables encode two hash values with fixed difference.

In the second TA-program (MD5_collision_second_block.alg) the initialization value should be set to hash values h1 and h1' obtained from finding the first blocks M0 and M0' (they correspond to the assignment of the last 256 variables in the satisfying assignment for the MD5_first_block.cnf). 
As an example, in MD5_collision_second_block.alg initialization values are the following:

	A1 = 0x2a9fdb43;
	B1 = 0x31a7e30e;
	C1 = 0x738cd43c;
	D1 = 0x619d892f;

	A2 = 0xaa9fdb43;
	B2 = 0xb3a7e30e;
	C2 = 0xf58cd43c;
	D2 = 0xe39d892f;

corresponding to the hash values for M0 and M0' message blocks: 

	M0:
	0x30000000 0xfaa6233c 0x587e91be 0xc77530f1
	0xc0be4200 0x645fb48b 0x666b1fde 0x4b5717b2
	0x05336d45 0xa1f46c02 0x787f0437 0x930a92a2
	0xcbeb4cca 0xd77daefd 0x3c979969 0xcba04ee2

	M0':
	0x30000000 0xfaa6233c 0x587e91be 0xc77530f1
	0x40BE4200 0x645fb48b 0x666b1fde 0x4b5717b2
	0x05336d45 0xa1f46c02 0x787f0437 0x930B12A2
	0xcbeb4cca 0xd77daefd 0xBC979969 0xcba04ee2

The result of the translation of MD5_collision_second_block.alg is MD5_second_block.cnf CNF, in which the first 512 variables encode the first message block M1 and the last 128 variables encode the coincidental hash value for message blocks M1 and M1'.

To check collision results one need to run the following commands via terminal:

> md5.exe Collision1_msg1.bin Collision1_msg2.bin

or

> md5sum Collision1_msg1.bin Collision1_msg2.bin

MD5 (Collision1_msg1.bin) = 178477e15fde4ff267aa55438d539b16

MD5 (Collision1_msg2.bin) = 178477e15fde4ff267aa55438d539b16

