MD5.alg program encodes MD5 hash function algorithm which transform one message block (512 bits).
The translation of this program results in MD5.cnf CNF-formula.
The original message (512 bits) is encoded with variables number 1 .. 512.
The hash value (128 bits) is encoded with variables number 16446 .. 16573.

To construct a cryptanalysis problem one needs to add to CNF MD5.cnf known hash value via unit clauses.
Preimage for the hadh value is the assignment of first 512 variables that can be extracted from the satisfying assignment of the CNF.

The folder MD5_collisions contains programs and encodings for the problem of finding two-blocks collisions for hash function MD5.