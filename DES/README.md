﻿Folder 64bit contains encodings of 2-16 stages of the DES algorithm applied to one block (64 bits) of plaintext. For example, 64bit/des_16_64.cnf is an encoding of 16-stage DES algorithm applied to 1 plaintext block.

Folder 128bit contains similar encodings of the DES algorithm applied to 2 blocks (128 bit) of plaintext.

Variables number 1 .. 56 encode a secret key.
Variables number 57 .. 120 encode 1st plaintext block (64 bits).
Variables number 121 .. 184 encode 2nd plaintext block (64 bits) in case of encodings from 128bit folder.

Ciphertext is encoded by last variables.
For example in 64bit/des_16_64.cnf variables number 1849 .. 1912 encode ciphertext; 
likewise in 128bit/des_16_128.cnf variables number 3641 .. 3768 encode ciphertext.

To construct a cryptanalysis problem one needs to add to this CNF known plaintext and corresponding ciphertext via unit clauses.
Secret key for this problem is the assignment of first 56 variables that can be extracted from the satisfying assignment of the CNF.