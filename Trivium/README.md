Trivium.alg program encodes the Trivium keystream generator which produced 300 keystream bits (with initialization step).
The translation of this program results in Trivium.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 288, in particular:
 - variables number 1 .. 93 encode values of cells of the first register (regA), 
   variable number 1 corresponds to cell regA[0], ..., variable number 93 corresponds to cell regA[92];
 - variables number 94 .. 177 encode values of cells of the second register(regB);
   variable number 94 corresponds to cell regB[0], ..., variable number 177 corresponds to cell regB[83];
 - variables number 178 .. 288 encode values of cells of the third register (regC);
   variable number 178 corresponds to cell regC[0], ..., variable number 288 corresponds to cell regC[110];

Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in Trivium.cnf keystream bits are encoded with variables number 4849, ..., 5148,  
where variable number 4849 encodes 1st keystream bit, ..., variable number 5148 encodes 200th keystream bit.

Trivium_no_init.alg program encodes the Trivium keystream generator which produced 300 keystream bits (without initialization step).
The translation of this program results in Trivium_no_init.cnf CNF-formula.
As it done before the initial state of the register cells is encoded with the first 177 variables with numbers 1 .. 288.
Keystream output is encoded with the last 300 variables with variables number 1588 .. 1887,
where variable number 1588 encodes 1st keystream bit and variable number 1887 encodes 200th keystream bit.

Transalg do not substitute into Trivium.cnf and Trivium_no_int.cnf the initial state of the register cells (key value, initial vector) and the bits of the key stream.
To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
