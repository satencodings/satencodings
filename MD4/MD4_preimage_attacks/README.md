Preimage attacks on truncated variants of MD4 hash fucntion using relaxation constraints

By MD4-k we denote a truncated variant of hash function defined by the first k steps of the MD4 algorithm.

The md4_32_preimage_attack_Dobbertin.alg TA-program describes the preimage finding problem for MD4-32 using additional constrains (called relaxation constraints) on chaining varables a,b,c,d at certain steps originally proposed by H. Dobbertin for the MD4-32 preimage finding problem. In this program we considered the variant if which value of constant K is fixed with zero 32-bit constant. The information about the known hash value is added using "assert" build-in Transalg function. Thus, the result of the TA-program translation is md4_32_preimage_attack_Dobbertin.cnf, in which the known hash value takes 128-bit zero value and corresponds to variables with number 5410 .. 5537.

The md4_39_preimage_attack_De.alg TA-program describes the preimage finding problem for MD4-39 using another set of relaxation constrains on chaining varables a,b,c,d proposed by K. De et. al for MD4-39 preimage finding problem for hash value of a special kind. The result of translation is md4_39_preimage_attack_De.cnf, in which the known hash value takes 128-bit nonzero value (according to De's MD4-39 preimage attack) and corresponds to variables with number 6712 .. 6839. 

In md4_39_preimage_attack_with_relaxation_constraits.alg possible variants of relaxation constraints with constant K = 0 are encoded using set of Boolean variables S called set of switching variables. If Boolean variable S[i] is equal to 1 then some corresponding relaxation constraint is active which means that one of four 32-bit chaining varables a,b,c or d is fixed with zero 32-bit constant.
We suggested two new sets of relaxation constrains for the MD4-39 preimage finding problem, where each set is defined using the following values of switching variables:

S[9] = S[10] = S[12] = S[13] = S[14] = S[16] = S[17] = S[18] = S[20] = S[21] = S[22] = S[24] = 1
S[10] = S[12] = S[13] = S[14] = S[16] = S[17] = S[18] = S[20] = S[21] = S[22] = S[24] = S[25] = 1

In md4_39_preimage_attack_with_relaxation_constraits.alg the hash value if not fixed. To construct the cryptanalysis problem for finding the MD4-39 preimage one should add the information about the known hash value via unit clauses in corresponding CNF md4_39_preimage_attack_with_relaxation_constraits.cnf or directly in the TA-program in the same manner as it is done in TA-programs for Dobbertin's and De's attacks. In md4_39_preimage_attack_with_relaxation_constraits.cnf the hash value corresponds to variables with number 6743 .. 6870.


