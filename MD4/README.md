﻿The MD4.alg program describes the algorithm computing the MD4 hash function, that transforms 1 message block (512 bit) to 128-bit hash value. 
The result of the translation of this program is the CNF MD4_1.cnf. 

In this CNF the original message block (512 bit) is encoded by the first 512 Boolean variables with numbers 1 .. 512. 
The corresponding hash value (128 bit) is encoded by the last 128 Boolean variables with numbers 8630 .. 8757.

To construct the CNF for finding the preimage one should add to the CNF the information about the known hash value (via unit clauses).
If the satisfying assignment is found then the preimage corresponds to the assignment of variables with numbers 1 .. 512.

The folder MD4_collisions contains programs and encodings for the problem of finding single-block collisions for hash function MD4.
The folder MD4_preimage_attacks contains programs and encodings for different variants of the preimage finding problem of function MD4.
