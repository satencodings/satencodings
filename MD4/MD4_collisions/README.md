The MD4_collision.alg program describes the problem of search for single-block collisions for the MD4 hash function. 
Essentially it implements the method proposed by X. Wang for the considered problem.
The result of the translation of this program is the CNF in which all necessary conditions (i.e. constraints on the differential path, etc.) are already present.
Therefore, one can immediately start solving it using a SAT solver. 

In the satisfying assignment the first message corresponds to the assignment of first 512 Boolean variables. 
To produce the second message (the collision) one needs to subtract the known constants (found in the work by X. Wang).

The result of the translation for MD4_collision.alg is MD4_collision.cnf.