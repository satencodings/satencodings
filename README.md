## About
SAT encodings for various cryptographic primitives constructed by Transalg (https://gitlab.com/transalg/transalg).

Each folder contains short description of a cryptographic primitive (or family of primitives), corresponding TA-programs and SAT encodings in DIMACS format produced by Transalg.

## TA language
In Transalg the input data is a text file containing a program written in a specialized programming language (TA-language). This program defines an algorithm which calculates some discrete function and called TA-program.

There are two main data types in TA language: *bit* and *int*. 
The *bit* data type is designed to work with symbolic data – *bit*-type variables are encoded using Boolean variables. Transalg implements the symbolic execution only on variables of *bit* type. 
The variables defining the input and output of the algorithm are always defined as *bit* variables, since they are included in a set of variables of symbolic execution. Additionally, there are special attributes __in (for declaring input variables) and __out (for output variables) which are necessary to create the corresponding code variables at the initial step of the symbolic execution.

	__in bit input[64];
	__out bit output[32];

In this example there are declared two *bit* vectors: first one is a 64-bit vector of Boolean variables, which represents the input of the algorithm (64-bit word or number), and second one is a 32-bit vector of Boolean variables that encode the output.

The *int* data type is designed to work with known numeric data (for example, loop counters or known algorithm parameters). 

A program in TA language consists of a set of procedures/functions, global variables and constants. The variables encoding the input and output of the algorithm are always declared as global, i.e. outside the body of functions and procedures. The symbolic execution starts with function main, which is a starting point (entry point) and essential part any TA program (similarly with the C language).

    void main()
    {
        // some operations
    }

The body of any function in TA-program is a list of operators. The TA language supports the basic operators of procedural programming languages:

- **variable declaration**
    ```
    bit x;
    int i = 0;
    ```
- **integer constant declaration** 
	```
    define i 5;
    ```
- **assignment operator** 
	```
    x = reg[i];
    ```
- **loop operator**
	```
    for (int i = 0; i < n; i = i + 1)
	{
	    // some operations
	}
	```
- **conditional operator**
	```
    bit condition;
	...
	if (condition)
	{
	    // some operations
	}
	else
	{
	    // some operations
	}
	```
-  **procedure/function call**
	```
    m = majority(x, y, z);
    ```

TA language supports basic arithmetic and logical operations. 

Bitwise logical operations: conjunction (&), disjunction (|), XOR operation or addition modulo 2 (^) and negation (!).

Integer operations: addition, subtraction, multiplication.

In addition, for vectors of type bit there are special build-in functions:
- sum(x, y, m) calculates the integer sum of x and y and returns the first m bits of this value.
- mul(x, y, m) calculates the integer product of x and y and returns the first m bits of this value.


## Examples
1. The linear feedback shift register (LFSR) is one of the common basic elements used in modern stream ciphers. Below is an example of a TA-program that implements the LFSR with the feedback function f (x) = x which generates 128 bits of the keystream.
    ```
	define len 128;
	__in bit reg[19];
	__out bit result[len];
	bit shift_rslos()
	{
		bit x = reg[18];
		bit y = reg[18] ^ reg[17] ^ reg[16] ^ reg[13];
		for(int j = 18; j > 0; j = j - 1)
		{
			reg[j] = reg[j - 1];
		}
		reg[0] = y;
		return x;
	}
	void main()
	{
		for(int i = 0; i < len; i = i + 1)
		{
			result[i] = shift_rslos();
		}
	}
    ```
2. This example demonstrates the application of conditional operators, which is a feature of the A5 /1 keystream generator:
    ```
	void main()
	{
		int midA = 8;
		int midB = 10;
		int midC = 10;
		bit maj;
		for(int i = 0; i < len; i= i + 1)
		{
			maj = majority(regA[midA],regB[midB],regC[midC]);
			if(!(maj^regA[midA])) shift_rslosA();
			if(!(maj^regB[midB])) shift_rslosB();
			if(!(maj^regC[midC])) shift_rslosC();                           
			result[i] = regA[18]^regB[21]^regC[22];
		}
	}
    ```
    Full example of the implementation of A5/1 including additional functions (majority, shift_rslosA, shift_rslosB, shift_rslosC) can be found at 
    https://gitlab.com/satencodings/satencodings/blob/master/A5_1/A5_1.alg

3. In addition to the register with linear feedback (LFSR) the Grain keystream generator uses a register with nonlinear feedback (NFSR). However, the feedback function is too complicated for effective processing by one formula. This example demonstrates how to break large logical expressions into parts by using additional variables with attribute __mem.
    ```
	void NFSR_shift()
	{
	__mem bit y1 = NFSR[52]&NFSR[45]&NFSR[37]&NFSR[33]&NFSR[28]&NFSR[21] ^ NFSR[33]&NFSR[28]&NFSR[21] ^ NFSR[37]&NFSR[33] ^ NFSR[52] ^ NFSR[45] ^ NFSR[37] ^ NFSR[33] ^ NFSR[28] ^ NFSR[21];
	__mem bit y2 = NFSR[33]&NFSR[28]&NFSR[21]&NFSR[15]&NFSR[9] ^ NFSR[15]&NFSR[9] ^ NFSR[14] ^ NFSR[9];
	__mem bit y3 = NFSR[63]&NFSR[60]&NFSR[52]&NFSR[45]&NFSR[37] ^ NFSR[60]&NFSR[52]&NFSR[45] ^ NFSR[60]&NFSR[52]&NFSR[37]&NFSR[33];
	__mem bit y4 = NFSR[63]&NFSR[60]&NFSR[21]&NFSR[15] ^ NFSR[63]&NFSR[60] ^ NFSR[62] ^ NFSR[60];
	__mem bit y5 =  NFSR[0] ^ NFSR[63]&NFSR[45]&NFSR[28]&NFSR[9];
	__mem bit y = y1 ^ y2 ^ y3 ^ y4 ^ y5;
	for(int j = 0; j < 79; j = j + 1)
	{
		NFSR[j] = NFSR[j + 1];
	}
	NFSR[79] = y ^ LFSR[0];
	}
    ```
    Full example of the implementation of the Grain keystream generator (version 1) can be found at 
    https://gitlab.com/satencodings/satencodings/blob/master/Grain/Grain_no_init_ver1.alg

4. In MD4 hash function the operations of integer addition and bitwise operations applied to integers are actively used. Each integer in the program is represented by a vector of bit type. The examples of the implementation of round functions used in MD4 are presented below.
    ```
	bit FF(bit a[32], bit b[32], bit c[32], bit d[32], bit M[32], int s)
	{
		a = sum(sum(a, F(b, c, d), 32), M, 32);
		return (a <<< s);
	}
	bit GG(bit a[32], bit b[32], bit c[32], bit d[32], bit M[32], int s)
	{
		a = sum(sum(sum(a, G(b, c, d), 32), M, 32), 0x5A827999, 32);
		return (a <<< s);
	}
	bit HH(bit a[32], bit b[32], bit c[32], bit d[32], bit M[32], int s)
	{
		a = sum(sum(sum(a, H(b, c, d), 32), M, 32), 0x6ED9EBA1, 32);
		return (a <<< s);
	}
	```    
    Full example of the implementation of MD4 can be found at https://gitlab.com/satencodings/satencodings/blob/master/MD4/MD4.alg