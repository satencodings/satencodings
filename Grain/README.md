Grain_no_init.alg program encodes the Grain generator outputting 160 bits of keystream (without initialization step).
The translation of this program results in Grain_no_init.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 160, in particular:
 - variables number 1 .. 80 encode values of cells of the nonlinear feedback shift register (NFSR), 
   variable number 1 corresponds to cell NFSR[0], ..., variable number 80 corresponds to cell NFSR[79];
 - variables number 81 .. 160 encode values of cells of the linear feedback shift register(LFSR);
   variable number 81 corresponds to cell LFSR[0], ..., variable number 160 corresponds to cell LFSR[79];
 
Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in Grain_no_init.cnf keystream bits are encoded with variables number 1786, ..., 1945, 
where variable number 1786 encodes 1st keystream bit, ..., variable number 1945 encodes 160th keystream bit.

To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
Secret key for this problem (initial values of registers cells) is the assignment of first 160 variables that can be extracted from 
the satisfying assignment of the CNF.