The tea.alg program describes the algorithm of cipher [TEA](https://ru.wikipedia.org/wiki/TEA) which encodes one 64-bit block of plaintext.
tea_tests.7z contains a template CNF (the result of the translation of tea.alg) and a series of test problems and corresponing triples (key values, plaintext values, ciphertext values) in separate files.
Test problems are CNFs with known plaintext and corresponding ciphertext added via unit clauses. 
In these CNFs:
* variables with number 1 .. 128 encode a secret key,
* variables number 129 .. 192 encode plaintext block,
* last 64 variables encode corresponding ciphertext (64 bits).

The xtea.alg program describes the algorithm of cipher [XTEA](https://ru.wikipedia.org/wiki/XTEA) which encodes one 64-bit block of plaintext.
xtea_tests.7z contains a template CNF (the result of the translation of xtea.alg) and a series of test problems and corresponing triples (key values, plaintext values, ciphertext values) in separate files.

Variable numbering is similar to the numbering used in TEA encodings.