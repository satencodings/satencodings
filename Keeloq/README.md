Keeloq.alg program encodes the Keeloq algorithm applied to 4 blocks of plaintext (the length of one block is 32 bits). 
The translation of this program results in geffe_100.cnf CNF-formula.
Changing the value of constant *blocks* one can produce encoding for the desired number of plaintext blocks. 

The keeloq-encoding.7z archive contains template CNFs representing the encodings for 1, 2, 3, and 4 blocks.

In these CNFs variables with numbers 1 .. 64 encode a secret key.
Variables encoding the plaintext starts from variable with number 65.

In case of 4 blocks of plaintext:
* first block is encoded with Boolean variables 65 .. 96,
* second block is encoded with Boolean variables 97 .. 128,
* third block is encoded with Boolean variables 129 .. 160,
* forth block is encoded with Boolean variables 161 .. 192.

Ciphertext output is always encoded by last Boolean variables.

To construct a cryptanalysis problem one needs to add to this CNF known plaintext and corresponding ciphertext via unit clauses.
Secret key for this problem is the assignment of first 64 variables that can be extracted from the satisfying assignment of the CNF.
