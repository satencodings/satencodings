e0_128.alg program encodes 128 steps of the E0 keystream generator which produced 128 keystream bits.
The translation of this program results in e0_128.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 132, in particular:
 - variables number 1 .. 25 encode values of cells of the first register(regA), 
   variable number 1 corresponds to cell regA[0], ..., variable number 25  corresponds to cell regA[24];
 - variables number 26 .. 56 encode values of cells of the second register (regB);
   variable number 26 corresponds to cell regB[0], ..., variable number 56 corresponds to cell regB[30];
 - variables number 57 .. 89 encode values of cells of the third register (regC);
   variable number 57 corresponds to cell regC[0], ..., variable number 89 corresponds to cell regC[32];
 - variables number 90 .. 128 encode values of cells of the fourth register (regD);
   variable number 90 corresponds to cell regD[0], ..., variable number 128 corresponds to cell regD[38];
 - variables number 129 .. 132 encode the carry cells (reg1 and reg2);

Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in e0_128.cnf keystream bits are encoded with variables number 1785, ..., 1912, 
where variable number 1785 encodes 1st keystream bit, ..., variable number 1912 encodes 128th keystream bit.

To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
Secret key for this problem is the assignment of first 128 variables that can be extracted from the satisfying assignment of the CNF.
