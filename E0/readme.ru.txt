﻿Программа e0_128.alg описывает работу генератора E0, выдающего 128 бит ключевого потока.
Результатом трансляции данной программы является формула e0_128.cnf.

Начальное состояние регистров кодируется булевыми переменными с номерами 1 .. 132, а именно:
 - переменные 1 .. 25 кодируют состояние первого регистра (ячейки regA), 
   переменная с номером 1 соответствует ячейке regA[0], а переменная с номером 25 - ячейке regA[24];
 - переменные 26 .. 56 кодируют состояние второго регистра (ячейки regB);
   переменная с номером 26 соответствует ячейке regB[0], а переменная с номером 56 - ячейке regB[30];
 - переменные 57 .. 89 кодируют состояние третьего регистра (ячейки regC);
   переменная с номером 57 соответствует ячейке regC[0], а переменная с номером 89 - ячейке regC[32];
 - переменные 90 .. 128 кодируют состояние четвертого регистра (ячейки regD);
   переменная с номером 90 соответствует ячейке regD[0], а переменная с номером 128 - ячейке regD[38];
 - переменные 129 .. 132 кодируют состояние сумматора (ячейки reg1 и reg2);

Выдаваемый генератором ключевой поток кодируются всегда последними переменными, в том порядке, в котором генератор выдает биты ключевого потока. 
Таким образом, в формуле e0_128.cnf ключевому потоку соответствуют переменные с номерами 1785, ..., 1912, 
где переменная с номером 1785 кодирует 1-й бит ключевого потока, ..., переменная с номером 1912 кодирует 128-й бит ключевого потока.

Для создания задачи криптоанализа нужно подставить в КНФ известный ключевой поток (однолитеральные дизъюнкты). 
Искомый ключ (начальное заполнение регистров) - это значения истинности переменных с номерами 1 .. 128, извлеченный из выполняющего набора построенной КНФ.
