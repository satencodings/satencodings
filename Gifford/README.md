﻿gifford.alg program encodes the Gifford generator which produced 20 bytes (160 bits) of a keystream.
The translation of this program results in gifford_20.cnf CNF-formula.

Registers of the generator consist of 8 cells with capacity of 1 byte each. 
In the TA program it is represented as a two-dimensional bitmap array reg.
Values of registers cells at the start are encoded with Boolean variables number 1 .. 64.
Variable number 1 corresponds to the first cell of the register reg[0][0], variable number 2 corresponds to the second cell of the register - reg[0][1], ...,
Variable number 8 corresponds to the reg[0][7], ..., variable number 64 corresponds to the reg[7][7].

Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in gifford_20.cnf keystream bits are encoded with variables number 8104, ..., 8263, 
where variable number 8104 encodes 1st keystream bit, ..., variable number 8263 encodes 160th keystream bit.

To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
Secret key (initial values of register cells) for this problem is the assignment of first 64 variables that can be extracted from the satisfying 
assignment of the CNF.