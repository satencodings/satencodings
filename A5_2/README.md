A5_2.alg program describes 128 steps of the A5/2 keystream generator which produced 128 keystream bits.
The translation of this program results in a5_2_128.cnf CNF-formula.

Values of registers cells at the start are encoded with Boolean variables number 1 .. 64, in particular:
 - variables number 1 .. 19 encode values of cells of the first register(regA), 
   variable number 1 corresponds to cell regA[0], ..., variable number 19 - to cell regA[18];
 - variables number 20 .. 41 encode values of cells of the second register(regB), 
   variable number 20 corresponds to cell regB[0], ..., variable number 41 - to cell regB[21];
 - variables number 42 .. 64 encode values of cells of the third register(regC), 
   variable number 42 corresponds to cell regC[0], ..., variable number 64 - to cell regC[22];
 - variables number 65 .. 81 encode values of cells of the fourth(control) register(regD), 
   variable number 65 corresponds to cell regD[0], ..., variable number 81 - to cell regD[16];
 
Keystream output is always encoded by last Boolean variables in the order in which generator creates corresponding keystream bits.
So in a5_2_128.cnf keystream bits are encoded with variables number 9047, ..., 9174, 
where variable number 9047 encodes 1st keystream bit, ... , variable number 9174 encodes 128th keystream bit.

To construct a cryptanalysis problem one needs to add to this CNF known keystream bits via unit clauses.
Secret key for this problem is the assignment of the first 81 variables that can be extracted from the satisfying assignment of the CNF.